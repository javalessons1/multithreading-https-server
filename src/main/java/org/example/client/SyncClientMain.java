package org.example.client;

import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.auth.AuthScope;
import org.apache.hc.client5.http.auth.UsernamePasswordCredentials;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.auth.BasicCredentialsProvider;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactory;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactoryBuilder;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

@Slf4j
public class SyncClientMain {
  public static void main(String[] args) throws NoSuchAlgorithmException, KeyManagementException,
          UnrecoverableKeyException, CertificateException, KeyStoreException, IOException {
    final HttpGet httpGet = new HttpGet("https://server.local:8443/apartments");
    // FIXME: just for demo (don't store passphrases in source code)
    System.setProperty("javax.net.ssl.keyStore", "web-certs/users/admin.jks");
    System.setProperty("javax.net.ssl.keyStorePassword", "passphrase");
    System.setProperty("javax.net.ssl.trustStore", "web-certs/truststore.jks");
    System.setProperty("javax.net.ssl.trustStorePassword", "passphrase");

    final SSLConnectionSocketFactory sslSocketFactory = SSLConnectionSocketFactoryBuilder.create()
        .useSystemProperties()
        .build();

    final PoolingHttpClientConnectionManager connectionManager = PoolingHttpClientConnectionManagerBuilder.create()
        .setSSLSocketFactory(sslSocketFactory)
        .build();

    final BasicCredentialsProvider provider = new BasicCredentialsProvider();
    provider.setCredentials(
        new AuthScope("server.local", 8443),
        new UsernamePasswordCredentials("masha", "secret".toCharArray())
    );

    try (
        final CloseableHttpClient client = HttpClients.custom()
            .setConnectionManager(connectionManager)
            .setDefaultCredentialsProvider(provider)
            .build();
        final CloseableHttpResponse response = client.execute(httpGet);
    ) {
      final String data = EntityUtils.toString(response.getEntity());
      log.debug("data: {}", data);

    } catch (IOException | ParseException e) {
      log.error("can't make request", e);
    }
  }
}
