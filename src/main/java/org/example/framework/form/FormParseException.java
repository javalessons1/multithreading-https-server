package org.example.framework.form;

import org.example.framework.exception.InvalidRequestStructureException;

public class FormParseException extends InvalidRequestStructureException {
    public FormParseException(String message) {
        super(message);
    }
}
