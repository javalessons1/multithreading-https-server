package org.example.framework.http;

import org.example.framework.exception.InvalidHeaderLineStructureException;
import org.example.framework.exception.InvalidRequestLineStructureException;
import org.example.framework.exception.InvalidRequestStructureException;
import org.example.framework.exception.RequestBodyTooLargeException;
import org.example.framework.parser.QueryParse;
import org.example.framework.util.Bytes;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

public class RequestParser {
    private static final int MAX_CONTENT_LENGTH = 1024 * 1024 * 10;
    private static final byte[] CRLF = new byte[]{'\r', '\n'};
    private static final byte[] doubleCRLF = new byte[]{'\r', '\n', '\r', '\n'};

    public static void parseRequest(final Request request, final InputStream in, final byte[] buffer) throws IOException {
        final int requestLineEndIndex = Bytes.indexOf(buffer, CRLF);
        final String requestLine = new String(buffer, 0, requestLineEndIndex, StandardCharsets.UTF_8);
        final String[] requestLineParts = requestLine.split("\\s+", 3);
        if (requestLineEndIndex == -1) {
            throw new InvalidRequestStructureException("request line end index not found");
        }
        if (requestLineParts.length != 3) {
            throw new InvalidRequestLineStructureException(requestLine);
        }
        final int headersStartIndex = requestLineEndIndex + CRLF.length;
        final int headersEndIndex = Bytes.indexOf(buffer, doubleCRLF, headersStartIndex);
        if (headersEndIndex == -1) {
            throw new InvalidRequestStructureException("header end index not found");
        }
        final String pathAndQuery = URLDecoder.decode(requestLineParts[1], StandardCharsets.UTF_8.name());
        final String[] pathAndQueryParts = pathAndQuery.split("\\?", 2);
        final String requestPath = pathAndQueryParts[0];
        request.setMethod(requestLineParts[0]);
        request.setHttpVersion(requestLineParts[2]);
        if (pathAndQueryParts.length == 2) {
            request.setQuery(pathAndQueryParts[1]);
            request.setQueryParams(QueryParse.parse(pathAndQueryParts[1]));
        }
        request.setPath(requestPath);
        int lastProcessedIndex = headersStartIndex;
        int contentLength = 0;
        Map<String, String> headers = new LinkedHashMap<>();
        while (lastProcessedIndex < headersEndIndex - CRLF.length) {
            final int currentHeaderEndIndex = Bytes.indexOf(buffer, CRLF, lastProcessedIndex);
            final String currentHeaderLine = new String(buffer, lastProcessedIndex, currentHeaderEndIndex - lastProcessedIndex);
            lastProcessedIndex = currentHeaderEndIndex + CRLF.length;
            final String[] headerParts = currentHeaderLine.split(":\\s*", 2);
            if (headerParts.length != 2) {
                throw new InvalidHeaderLineStructureException(currentHeaderLine);
            }
            if (headerParts[0].equalsIgnoreCase(HttpHeaders.CONTENT_TYPE.value())) {
                String contentType = headerParts[1].split(";")[0];
                request.setContentType(contentType);
            }

            headers.put(headerParts[0], headerParts[1]);
            if (!headerParts[0].equalsIgnoreCase(HttpHeaders.CONTENT_LENGTH.value())) {
                continue;
            }
            contentLength = Integer.parseInt(headerParts[1]);
        }
        request.setHeaders(headers);
        if (contentLength > MAX_CONTENT_LENGTH) {
            throw new RequestBodyTooLargeException();
        }
        final int bodyStartIndex = headersEndIndex + doubleCRLF.length;
        in.reset();
        in.skip(bodyStartIndex);
        final byte[] body = new byte[contentLength];
        in.read(body);
        request.setBody(body);
    }
}
