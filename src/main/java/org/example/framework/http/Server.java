package org.example.framework.http;

import lombok.Builder;
import lombok.Singular;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.auth.SecurityContext;
import org.example.framework.exception.*;
import org.example.framework.handler.Handler;
import org.example.framework.middleware.Middleware;
import javax.net.ServerSocketFactory;
import javax.net.ssl.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Builder
public class Server {
    private static final int MAX_REQUEST_LINE_AND_HEADERS_SIZE = 4096;
    private static final long SHUTDOWN_TIME = 5000L;
    private final AtomicInteger workerCounter = new AtomicInteger();
    private final ExecutorService workers = Executors.newFixedThreadPool(64, r -> {
        final Thread worker = new Thread(r);
        worker.setName("worker-" + workerCounter.incrementAndGet());
        return worker;
    });

    @Builder.Default
    private final AtomicBoolean running = new AtomicBoolean(false);
    @Singular
    private final List<Middleware> middlewares;
    @Singular
    private final Map<Pattern, Map<HttpMethods, Handler>> routes;
    @Builder.Default
    private final Handler notFoundHandler = Handler::notFoundHandler;
    @Builder.Default
    private final Handler methodNotAllowed = Handler::methodNotAllowedHandler;
    @Builder.Default
    private final Handler internalServerErrorHandler = Handler::internalServerError;

    private ServerSocket serverSocket;
    private final ExecutorService executor = Executors.newFixedThreadPool(1, r -> {
        final Thread mainThread = new Thread(r);
        mainThread.setName("mainThread");
        return mainThread;
    });

    public void serveHTTPS(final int port) throws SocketException {
        try {
            final ServerSocketFactory socketFactory = SSLServerSocketFactory.getDefault();
            serverSocket = socketFactory.createServerSocket(port);
            final SSLServerSocket sslServerSocket = (SSLServerSocket) serverSocket;
            sslServerSocket.setEnabledProtocols(new String[]{"TLSv1.2"});
            sslServerSocket.setWantClientAuth(true);
            log.info("server listen on {}", port);
            while (true) {
                final Socket socket = serverSocket.accept();
                workers.submit(() -> handle(socket));
            }
        }
        catch (SocketException e){
            stopExecutorService(workers);
            log.info("Socket is closed, workers stopped!");
            throw  new SocketException();
        }
        catch (IOException e){
            log.debug("IOException error!");
        }

    }
    public void start(int port) {
        executor.submit(() -> {
            try {
                serveHTTPS(port);
            } catch (SocketException e) {
                stopExecutorService(executor);
                log.info("Server stopped!");
            }
        });
    }
    public void stop(){
        try {
            log.info("Server stopping...");
            serverSocket.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopExecutorService(ExecutorService service) {
        service.shutdown();
        try {
            if (!service.awaitTermination(500, TimeUnit.MILLISECONDS)) {
                service.shutdownNow();
                if (!service.awaitTermination(500, TimeUnit.MILLISECONDS)) {
                    System.err.println("Pool did not terminate");
                }
            }
        } catch (InterruptedException e) {
            service.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    private void handle(final Socket acceptedSocket) {
        try (
                final Socket socket = acceptedSocket;
                final InputStream in = new BufferedInputStream(socket.getInputStream());
                final OutputStream out = socket.getOutputStream();
        ) {
            log.debug("client connected: {}:{}", socket.getInetAddress(), socket.getPort());
            final Request request = new Request();
            try {
                final byte[] buffer = new byte[MAX_REQUEST_LINE_AND_HEADERS_SIZE];
                if (!in.markSupported()) {
                    throw new MarkNotSupportedException();
                }
                in.mark(MAX_REQUEST_LINE_AND_HEADERS_SIZE);
                in.read(buffer);
                RequestParser.parseRequest(request, in, buffer);

                for (final Middleware middleware : middlewares) {
                    middleware.handle(socket, request);
                }
                Map<HttpMethods, Handler> methodToHandlers = null;
                for (final Map.Entry<Pattern, Map<HttpMethods, Handler>> entry : routes.entrySet()) {
                    final Matcher matcher = entry.getKey().matcher(request.getPath());
                    if (!matcher.matches()) {
                        continue;
                    }
                    request.setPathMatcher(matcher);
                    methodToHandlers = entry.getValue();
                }
                    if (methodToHandlers == null) {
                        throw new MethodNotAllowedException(request.getMethod());
                    }
                    final HttpMethods hh = HttpMethods.valueOf(request.getMethod());
                    final Handler handler = methodToHandlers.get(hh);
                    if (handler == null) {
                        throw new ResourceNotFoundException(request.getPath());
                    }
                    handler.handle(request, out);
                } catch (MethodNotAllowedException e) {
                    log.error("request method not allowed", e);
                    methodNotAllowed.handle(request, out);
                } catch (ResourceNotFoundException e) {
                    log.error("can't found request", e);
                    notFoundHandler.handle(request, out);
                } catch (Exception e) {
                    log.error("can't handle request", e);
                    internalServerErrorHandler.handle(request, out);
                }
        } catch (Exception e) {
            log.error("can't handle request", e);
        } finally {
            SecurityContext.clear();
        }
    }
}
