package org.example.framework.multipart;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.http.Request;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class MultipartParser {
    public static Map<String, List<Part>> parseBody(final String body, final Request request) {
        Map<String, List<Part>> multipartParts = new LinkedHashMap<>();
        try {
            if (request.getContentType().equals("multipart/form-data")) {
                final String[] getBoundary = body.split("\r\n");
                final String boundary = getBoundary[0];
                final String[] parts = body.split(boundary);
                for (final String part : parts) {
                    String name = null;
                    String[] namesLine;
                    String value;
                    Map<String, String> headers = new LinkedHashMap<>();
                    final String[] partElems = part.split("\r\n\r\n");
                    if (partElems.length > 1) {
                        final String[] headersToSplit = partElems[0].split("\r\n");
                        String headersKey;
                        String headersValue;
                        for (final String headerPart : headersToSplit) {
                            final String[] headerSplited = headerPart.split(":");
                            if (headerSplited.length > 1) {
                                headersKey = headerSplited[0];
                                final String[] headersValueLine = headerSplited[1].split(";");
                                headersValue = headersValueLine[0].replaceAll(" ", "");
                                headers.put(headersKey, headersValue);
                            }
                        }
                        namesLine = headersToSplit[1].split(";");
                        value = partElems[1];

                        for (final String nam : namesLine) {
                            List<Part> tempList = new ArrayList<>();
                            Part newFormPart;
                            Part newFilePart;
                            if (nam.contains(" name=")) {
                                name = nam.split("=")[1].replaceAll("\"", "");
                                if (multipartParts.containsKey(name)) {
                                    tempList = multipartParts.get(name);
                                    newFormPart = new FormFieldPart(name, value, headers);
                                    tempList.add(newFormPart);
                                } else {
                                    newFormPart = new FormFieldPart(name, value, headers);
                                    tempList.add(newFormPart);
                                }
                                multipartParts.put(name, tempList);
                            }
                            if (nam.contains(" filename=")) {
                                final String[] filenameString = nam.split("=");
                                final String filename1 = filenameString[1].replaceAll("\"", "");
                                if (filename1.equals("")) {
                                    newFilePart = new FilePart(name, "", null, headers);
                                } else {
                                    final byte[] content = value.getBytes();
                                    newFilePart = new FilePart(name, filename1, content, headers);
                                    tempList = multipartParts.get(name);
                                    tempList.remove(tempList.size() - 1);
                                }
                                tempList.add(newFilePart);
                                multipartParts.put(name, tempList);
                            }
                        }
                    }
                }
                return multipartParts;
            }
            return multipartParts;
        } catch (Exception e) {
            throw new MultipartParseException();
        }
    }
}
